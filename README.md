django-autopages
================

Application for sending SMS-messages using Stramedia service.

Installation
------------

1. Install package from repository:

        pip install -e git+ssh://github.com/onzuka/django-stramedia#egg=django-stramedia

2. In your `settings.py` add `'stramedia'` to `INSTALLED_APPS` and specify username/password
from your Stramedia account in dictionary:

        STRAMEDIA_CONF = {
            'username': '',             # Username from your account on Stramedia.
            'password': '',             # Password from your account on Stramedia.
            # Uncomment variables below if you want to run application tests.
            #'test_sender': 'NRTests',  # Ensure this name exists in Stramedia sender list.
            #'test_receiver': '',       # Set real phone number here.
        }

3. Run `python manage.py syncdb` to create tables for models.

Requirements
------------
- Python 2.7;
- Django>=1.4.2.

Running tests
-------------

1. Clone repository:

        mkdir stramedia
        cd stramedia
        git clone ssh://github.com/onzuka/django-stramedia .

2. Make local copy of test settings:

        cp test_settings.py.sample test_settings.py

Ensure that `'test_sender'` and `'test_receiver'` are set in `STRAMEDIA_CONF` dictionary and valid.

4. Install test requirements:

        pip install -r test_requirements.txt

5. Run tests with command:

        ./runtests.py

License
=======

Copyright © 2013, onzuka  
Copyright © 2013, http://github.com/onzuka/django-stramedia  
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

