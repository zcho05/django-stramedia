# vim: set fileencoding=utf-8 fileformat=unix :

from distutils.core import setup


setup(
    name='django-stramedia',
    version='0.1',
    packages=[
        'stramedia',
    ],
    install_requires=[
        'Django',
        'unidecode',
    ],
    description='Application for sending SMS messages using Stramedia',
    zip_safe=False,
)
