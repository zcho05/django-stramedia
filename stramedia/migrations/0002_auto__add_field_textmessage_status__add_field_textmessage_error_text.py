# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'TextMessage.status'
        db.add_column('stramedia_textmessage', 'status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=-1),
                      keep_default=False)

        # Adding field 'TextMessage.error_text'
        db.add_column('stramedia_textmessage', 'error_text',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'TextMessage.status'
        db.delete_column('stramedia_textmessage', 'status')

        # Deleting field 'TextMessage.error_text'
        db.delete_column('stramedia_textmessage', 'error_text')


    models = {
        'stramedia.textmessage': {
            'Meta': {'object_name': 'TextMessage'},
            'coding': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deferred': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'dlrmask': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'error_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mclass': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'response_text': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'sms_ids': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '64', 'blank': 'True'}),
            'sms_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '765'}),
            'to': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['stramedia']