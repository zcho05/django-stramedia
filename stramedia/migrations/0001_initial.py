# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TextMessage'
        db.create_table('stramedia_textmessage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('to', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('sender', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=765)),
            ('coding', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('priority', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('mclass', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('dlrmask', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('deferred', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('sms_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('sms_ids', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=64, blank=True)),
            ('response_text', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal('stramedia', ['TextMessage'])


    def backwards(self, orm):
        # Deleting model 'TextMessage'
        db.delete_table('stramedia_textmessage')


    models = {
        'stramedia.textmessage': {
            'Meta': {'object_name': 'TextMessage'},
            'coding': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deferred': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'dlrmask': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mclass': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'response_text': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'sms_ids': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '64', 'blank': 'True'}),
            'sms_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '765'}),
            'to': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['stramedia']