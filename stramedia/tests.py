# vim: set fileencoding=utf-8 fileformat=unix :

from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase
from django.test.utils import override_settings

from stramedia.sms import (
    send_sms,
    _parse_response,
)
from stramedia.models import TextMessage
from stramedia.conf import get_setting


class SendSMSTest(TestCase):

    def setUp(self):
        self.sender = get_setting('test_sender')
        self.correct_number = get_setting('test_receiver')
        self.to = [self.correct_number]

    def test_sending_latin_message_with_latin_coding_successful(self):
        text = 'Test latin message with latin coding.'
        self.assertFalse(TextMessage.objects.exists())
        send_sms(self.to, self.sender, text, coding=TextMessage.LATIN_CODING)
        self.assertEquals(1, TextMessage.objects.count())
        msg = TextMessage.objects.all()[0]
        self.assertEquals(self.correct_number, msg.to)
        self.assertEquals(self.sender, msg.sender)
        self.assertEquals(text, msg.text)
        self.assertEquals(1, msg.sms_number)

    def test_unicode_data(self):
        text = u'Сообщение в Unicode, test_unicode_data.'
        to = [self.to[0].decode('UTF-8')]
        sender = self.sender.decode('UTF-8')
        send_sms(to, sender, text)
        msg = TextMessage.objects.all()[0]
        self.assertEquals(TextMessage.SENT_TO_STRAMEDIA, msg.status)

    def test_sending_unicode_message_with_cyrillic_coding_successful(self):
        text = u"Тестовое сообщение в Unicode с кириличeской кодировкой."
        send_sms(self.to, self.sender, text, coding=TextMessage.CYRILLIC_CODING)
        msg = TextMessage.objects.all()[0]
        self.assertEquals(1, msg.sms_number)

    def test_sending_with_nonexisting_coding_fails(self):
        text = 'Test message with nonexisting coding.'
        self.assertFalse(TextMessage.objects.exists())
        invalid_coding = 100
        send_sms(
            self.to, self.sender, text, coding=invalid_coding)
        self.assertEquals(1, TextMessage.objects.count())
        msg = TextMessage.objects.all()[0]
        self.assertEquals(invalid_coding, msg.coding)
        self.assertEquals(0, msg.sms_number)

    def test_mass_sending_successful(self):
        text = 'Mass message sending test (one of two messages).'
        to = self.to * 2
        self.assertFalse(TextMessage.objects.exists())
        send_sms(to, self.sender, text)
        self.assertEquals(2, TextMessage.objects.count())

    def test_sending_of_splited_message_successful(self):
        text = (
            'Sending of a '
            'very-very-very-very-very-very-very-very-very '
            'very-very-very-very-very-very-very-very-very '
            'long message test.'
        )
        self.assertTrue(len(text) > 100)
        to = self.to
        self.assertFalse(TextMessage.objects.exists())
        send_sms(to, self.sender, text)
        self.assertEquals(1, TextMessage.objects.count())
        msg = TextMessage.objects.all()[0]
        self.assertEquals(msg.sms_number, 2)
        self.assertEquals(2, len(msg.sms_ids.split(',')))

    @override_settings(STRAMEDIA_CONF={'url': ''})
    def test_raises_exception_when_no_service_url_in_settings(self):
        self.assertRaises(ImproperlyConfigured, send_sms, [], '', '')

    @override_settings(STRAMEDIA_CONF={})
    def test_raises_exception_when_no_username_in_settings(self):
        self.assertRaises(ImproperlyConfigured, send_sms, [], '', '')

    def test_strips_and_splits_text(self):
        text = u'   Текст  с лишними  пробелами. test_strips_and_splits_text.'
        send_sms(self.to, self.sender, text)
        msg = TextMessage.objects.all()[0]
        self.assertEquals(
            u'Текст с лишними пробелами. test_strips_and_splits_text.',
            msg.text)

    def test_raises_value_error_when_required_parameter_is_empty(self):
        self.assertRaises(ValueError, send_sms, self.to, '', 'Test text.')
        self.assertRaises(ValueError, send_sms, self.to, self.sender, '')

    def test_sending_exceeding_length_message_fails(self):
        text = (
            'Test sending message with exceeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
            'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeding length.'
        )
        self.assertTrue(len(text) > TextMessage.MAX_LATIN_TEXT_LENGTH, len(text))
        send_sms(self.to, self.sender, text, coding=TextMessage.LATIN_CODING)
        self.assertEquals(1, TextMessage.objects.count())
        msg = TextMessage.objects.all()[0]
        self.assertEquals(0, msg.sms_number, msg.response_text)

    def test_send_sms_to_empty_whitelist(self):
        text = \
            "test_send_sms_to_empty_whitelist. " \
            "You should not get this message, test failed."
        from django.conf import settings
        settings.STRAMEDIA_CONF['whitelist'] = []
        send_sms(self.to, self.sender, text)
        msg = TextMessage.objects.all()[0]
        self.assertEquals(1, TextMessage.objects.count())
        self.assertEquals(0, msg.sms_number)
        self.assertEquals(TextMessage.NOT_SENT, msg.status)
        self.assertEquals('', msg.response_text)

    def test_send_sms_to_whitelist(self):
        text = "test_send_sms_non_empty_whitelist. "
        from django.conf import settings
        settings.STRAMEDIA_CONF['whitelist'] = self.to
        send_sms(self.to, self.sender, text)
        self.assertEquals(1, TextMessage.objects.count())
        msg = TextMessage.objects.all()[0]
        self.assertEquals(TextMessage.SENT_TO_STRAMEDIA, msg.status)

    def test_encodes_sms_with_latin_coding(self):
        text = u'Кириллица в ASCII. test_encodes_sms_with_latin_coding.'
        send_sms(self.to, self.sender, text, coding=TextMessage.LATIN_CODING)
        msg = TextMessage.objects.all()[0]
        latin_text = 'Kirillitsa v ASCII. test_encodes_sms_with_latin_coding.'
        self.assertEquals(latin_text, msg.text)


class ParseResponseTest(TestCase):

    def setUp(self):
        self.html = """
            <HTML>
            <HEAD>
            </HEAD>
            <BODY>
            %s
            </BODY>
            </HTML>
        """

    def test_error_response(self):
        message = "Error: Invalid or missing 'to' address"
        resp_text = self.html % message
        self.assertEquals(
            [{'response_text': message}], _parse_response(resp_text))

    def test_success_response(self):
        message = "Success: Message accepted for sending <BR>No. of sms: 1<BR>ID: 757741374"
        resp_text = self.html % message
        response_dicts = [{
            'response_text': 'Success: Message accepted for sending',
            'sms_ids': '757741374',
            'sms_number': 1,
        }]
        self.assertEquals(response_dicts, _parse_response(resp_text))

    def test_success_splitted_response(self):
        message = "Success: Message accepted for sending <BR>No. of sms: 2<BR>ID: 75774,1374"
        resp_text = self.html % message
        response_dicts = [{
            'response_text': 'Success: Message accepted for sending',
            'sms_ids': '75774,1374',
            'sms_number': 2,
        }]
        self.assertEquals(response_dicts, _parse_response(resp_text))

    def test_mixed_response(self):
        message = (
            "Error: Invalid or missing 'to' address<BR><BR>"
            "Success: Message accepted for sending <BR>No. of sms: 1<BR>ID: 757741374"
        )
        resp_text = self.html % message
        response_dicts = [
            {
                'response_text': "Error: Invalid or missing 'to' address",
            },
            {
                'response_text': 'Success: Message accepted for sending',
                'sms_ids': '757741374',
                'sms_number': 1,
            }
        ]
        self.assertEquals(response_dicts, _parse_response(resp_text))

    def test_not_html_text(self):
        resp_text = "Not HTML document"
        self.assertRaises(ValueError, _parse_response, resp_text)

    def test_invalid_success_response(self):
        message = "Success: Message accepted for sending <BR>No. of sms: -<BR>ID: 757a41374"
        resp_text = self.html % message
        self.assertRaises(ValueError, _parse_response, resp_text)
