# vim: set fileencoding=utf-8 fileformat=unix :

import re
import urllib2

from django.core.exceptions import ImproperlyConfigured
from traceback import format_exc
from unidecode import unidecode
from urllib import urlencode

from .encode import encode_dict
from .models import TextMessage
from .conf import get_setting


def send_sms(
        to, sender, text,
        coding=TextMessage.CYRILLIC_CODING,
        priority=TextMessage.DEFAULT_PRIORITY,
        mclass=TextMessage.USUAL_CLASS,
        dlrmask=TextMessage.DLR_NOTIFICATION_ON,
        deferred=None):
    """
    Send SMS message.

    Arguments:
    to - List of recipients' phone numbers.
    sender - Sender phone number or name.
    text - Message text.
    """
    url = get_setting('url')
    if not url:
        raise ImproperlyConfigured('Incorrect SMS service URL: %s.' % url)
    username = get_setting('username')
    password = get_setting('password')
    if not (username and password):
        raise ImproperlyConfigured(
            'Usename and password for Stramedia are required.')
    to = filter(None, to)
    if not to:
        return
    if not sender:
        raise ValueError('Missing sender.')
    if not text:
        raise ValueError('Missing text.')
    whitelist = get_setting('whitelist')
    receivers = to if whitelist is None else filter(lambda n: n in whitelist, to)
    text = u' '.join(text.strip().split())   # Encode text and clean up whitespaces.
    if coding == TextMessage.LATIN_CODING:
        text = unidecode(text)
    message_data = {
        'text': text,
        'priority': priority,
        'coding': coding,
        'mclass': mclass,
        'dlrmask': dlrmask,
        'deferred': deferred,
    }
    if receivers:
        request = urllib2.Request(url)
        data = {
            'username': username,
            'password': password,
            'from': sender,
            'to': u','.join(to),
        }
        data.update(message_data)
        data['deferred'] = '' if deferred is None else deferred
        request.add_data(urlencode(encode_dict(data)))
        # Sent POST-request for sending SMS-messages.
        try:
            response = urllib2.urlopen(request)
            resp_text = response.read()
        except Exception, e:
            error_text = format_exc(e)
            status = TextMessage.NOT_SENT
            responses = [{}] * len(receivers)
        else:
            error_text = ''
            status = TextMessage.SENT_TO_STRAMEDIA
            responses = _parse_response(resp_text)
        # Create and save sent messages in DB.
        for phone_number, dict_ in zip(receivers, responses):
            dict_.update(message_data)
            dict_['status'] = status
            dict_['error_text'] = error_text
            _log(sender=sender, to=[phone_number], **dict_)
    # Create and save excluded messages in DB.
    dict_ = {'status': TextMessage.NOT_SENT}
    dict_.update(message_data)
    excluded = set(to).difference(receivers)
    _log(sender=sender, to=excluded, **dict_)


def _log(to, **kwargs):
    for phone_number in to:
        TextMessage.objects.create(to=phone_number, **kwargs)


def _parse_response(html):
    """
    Parse HTML response from Stramedia.
    """
    if not html:
        return []
    match = re.search('BODY>\s*(?P<body>.*)\s*</BODY', html)
    if match is None:
        raise ValueError('Body not found in document: %s' % html)
    body = match.group('body')
    responses = body.split('<BR><BR>')
    dicts = []
    message_pattern = re.compile(
        '(?P<response_text>.*?)\s*<BR>\s*No. of sms: (?P<sms_number>\d+)\s*<BR>\s*ID: (?P<sms_ids>[\d,]+)')
    for response in responses:
        try:
            # Success response.
            dict_ = message_pattern.search(response).groupdict()
            dict_['sms_number'] = int(dict_['sms_number'])
            dicts.append(dict_)
        except AttributeError:
            # Error resonse should be single lined.
            if '<BR>' in response:
                raise ValueError('Unknown response format: %s' % response)
            dicts.append({'response_text': response})
    return dicts
