# vim: set fileencoding=utf-8 fileformat=unix :

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator


class TextMessage(models.Model):

    """
    Text message sent with SMS.
    """

    LATIN_CODING = 0
    BINARY_CODING = 1
    CYRILLIC_CODING = 2
    CODING_CHOICES = (
        (LATIN_CODING, 'Latin'),
        (BINARY_CODING, 'Binary'),
        (CYRILLIC_CODING, 'Cyrillic'),
    )
    USUAL_CLASS = 1
    MCLASS_CHOICES = (
        (0, 'Flash'),
        (USUAL_CLASS, 'Usual'),
    )
    DLR_NOTIFICATION_ON = 31
    DLR_NOTIFICATION_OFF = 0
    DLRMASK_CHOICES = (
        (DLR_NOTIFICATION_OFF, 'OFF'),
        (DLR_NOTIFICATION_ON, 'ON'),
    )
    NOT_SENT = -2
    SENT_TO_STRAMEDIA = -1
    STATUS_CHOICES = (
        (NOT_SENT, 'Not sent'),
        (SENT_TO_STRAMEDIA, 'Sent to Stramedia'),
    )
    DEFAULT_PRIORITY = 0

    MAX_LATIN_TEXT_LENGTH = 765
    MAX_CYRILLIC_TEXT_LENGTH = 335

    to = models.CharField(_('Receiver phone number'), max_length=15)
    sender = models.CharField(_('Sender phone number or name'), max_length=15)
    text = models.CharField(
        max_length=MAX_LATIN_TEXT_LENGTH, # Max number of latin letters.
        verbose_name=_('Message text'))
    coding = models.PositiveSmallIntegerField(
        _('Coding (latin, binary or cyrillic)'), choices=CODING_CHOICES)
    priority = models.PositiveSmallIntegerField(
        _('Priority (0-3)'), validators=[MaxValueValidator(3)])
    mclass = models.PositiveSmallIntegerField(
        _('Message class (flash or usual)'), choices=MCLASS_CHOICES)
    dlrmask = models.PositiveSmallIntegerField(
        _('Notify about delivery (0 - OFF, 31 - ON)'), choices=DLRMASK_CHOICES)
    deferred = models.PositiveSmallIntegerField(
        _('Delay in minutes before sending message'), null=True)

    sms_number = models.PositiveSmallIntegerField(
        _('Number of SMS used for sending a message'), default=0)
    sms_ids = models.CommaSeparatedIntegerField(
        _('Identifiers of SMS used for sending a message'),
        # Length of ID (simply large enough) * Max num of IDs + Max num of commas.
        max_length=12 * 5 + 4,
        blank=True)
    response_text = models.CharField(
        _('Message with error or success text from Stramedia'), max_length=60)
    status = models.SmallIntegerField(
        _('Message status'), choices=STATUS_CHOICES)
    error_text = models.TextField(
        _('Text of exception, if happened during request'), blank=True)

    class Meta:
        verbose_name = _('text message')
        verbose_name_plural = _('text messages')

    def __unicode__(self):
        return _('To {to} from {sender}: {text}').format(
            to=self.to, sender=self.sender, text=self.text)
