# vim: set fileencoding=utf-8 fileformat=unix :

from django.utils.encoding import smart_str


def encode_dict(d):
    """
    Encode unicode keys and values using smart_str.
    """
    encd = {}
    for key in d.keys():
        value = smart_str(d[key])
        encd[smart_str(key)] = value
    return encd
