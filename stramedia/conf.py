# vim: set fileencoding=utf-8 fileformat=unix :

from django.conf import settings


def get_setting(name):
    """
    Get project or application default setting by name.
    """
    project_conf = getattr(settings, 'STRAMEDIA_CONF', {})
    STRAMEDIA_URL = 'https://www.stramedia.ru/modules/send_sms.php'
    application_conf = {
        'url': project_conf.get('url', STRAMEDIA_URL),
        'username': project_conf.get('username', ''),
        'password': project_conf.get('password', ''),
        'whitelist': project_conf.get('whitelist'),
        'test_sender': project_conf.get('test_sender', ''),
        'test_receiver': project_conf.get('test_receiver', ''),
    }
    return application_conf[name]
