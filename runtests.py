#!/usr/bin/env python

import inspect
import sys

import test_settings

from os.path import dirname, abspath
from optparse import OptionParser

from django.conf import settings, global_settings


if not settings.configured:
    is_const = lambda p: p[0].isupper()
    settings_dict = dict(filter(is_const, inspect.getmembers(test_settings)))
    settings.configure(**settings_dict)


from django.test.simple import DjangoTestSuiteRunner


def runtests(*test_args, **kwargs):
    if not test_args:
        test_args = ['stramedia']
    parent = dirname(abspath(__file__))
    sys.path.insert(0, parent)
    test_runner = DjangoTestSuiteRunner(verbosity=kwargs.get('verbosity', 1), interactive=kwargs.get('interactive', False), failfast=kwargs.get('failfast'))
    failures = test_runner.run_tests(test_args)
    sys.exit(failures)

if __name__ == '__main__':
    parser = OptionParser()
    (options, args) = parser.parse_args()
    runtests(*args)
